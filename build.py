#!/usr/bin/python3

import os
import sys
import subprocess
import zipfile
import datetime

import xml.etree.ElementTree as ET

includes = [
    "res"
]

class Meta():
    def __init__(self, id: str, version: str, displayname: str, description: str) -> None:
        self.id = id
        self.version = version
        self.displayname = displayname
        self.description = description

    def __str__(self) -> str:
        return f"<Mod Meta: ID: {self.id} Version: {self.version} Display Name: {self.displayname} Description: {self.description}>"

    def __eq__(self, __o: object) -> bool:
        if isinstance(__o, type(self)):
            return self.id == __o.id and self.version == __o.version and self.displayname == __o.displayname and self.description == __o.description
        else:
            return NotImplemented

    @classmethod
    def fromxml(cls, xmlpath: str) -> "Meta":

        tree = ET.parse(xmlpath)
        root = tree.getroot()

        for child in root:
            match child.tag:
                case "id":
                    id = child.text
                case "version":
                    version = child.text
                case "name":
                    name = child.text
                case "description":
                    description = child.text

        return cls(id, version, name, description)

    @classmethod
    def fromstr(cls, metastr: str) -> "Meta":

        root = ET.fromstring(metastr)

        for child in root:
            match child.tag:
                case "id":
                    id = child.text
                case "version":
                    version = child.text
                case "name":
                    name = child.text
                case "description":
                    description = child.text

        return cls(id, version, name, description)

def py2_compile(files: list[str]) -> None:
    """call python2 to compile files to bytecode
    """

    if os.name == "nt":
        cmd = ["py", "-2", "-m", "compileall"]
    else:
        cmd = ["python2", "-m", "compileall"]
    for file in files:
        cmd.append(file)
    ret = subprocess.run(cmd)
    ret.check_returncode()

def package_mod(metadata: Meta):

    now = tuple(datetime.datetime.now().timetuple())[:6]
    zippath = metadata.id + "_" + metadata.version + ".wotmod"


    with zipfile.ZipFile(zippath, "w", compression=zipfile.ZIP_STORED) as z:
        f = open("meta.xml", "rb")
        info = zipfile.ZipInfo("meta.xml", now)
        info.external_attr = 33206 << 16 # -rw-rw-rw-
        z.writestr(info, f.read())
        f.close()
        
        for include in includes:
            for root, dirs, files in os.walk(include):
                for file in files:
                    if file.endswith(".py") == False:
                        fpath = os.path.join(root, file)
                        f = open(fpath, "rb")
                        info = zipfile.ZipInfo(fpath, now)
                        info.external_attr = 33206 << 16 # -rw-rw-rw-
                        z.writestr(info, f.read())
                        f.close()

def isnew():
    """use git to check if latest commit bumped version
    """
    curentmeta = Meta.fromxml("meta.xml")

    cmd = "git show HEAD~1:meta.xml | cat"
    ret = subprocess.check_output(cmd, shell=True)
    oldmeta = Meta.fromstr(ret)

    return curentmeta != oldmeta

def getversion():
    meta = Meta.fromxml("meta.xml")
    return meta.version

if "isnew" in sys.argv:
    print(isnew())
elif "version" in sys.argv:
    print(getversion())
else:

    modmeta = Meta.fromxml("meta.xml")
    print(modmeta)

    py2_compile(["res"])

    package_mod(modmeta)